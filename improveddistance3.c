#include<stdio.h>
#include<stdlib.h>
#include<math.h>

struct Point
{
    float x,y;
};

float getDistance(struct Point a,struct Point b)
{
  float distance;
    distance=sqrt((a.x-b.x)*(a.x-b.x)+ (a.y-b.y)*(a.y-b.y));
    return distance;
}

int main()
{
    struct Point a,b;
    printf("enter the co-ordinates of a:\n");
    scanf("%f%f",&a.x,&b.x);
    printf("enter the co-ordinates of b:\n");
    scanf("%f%f",&a.y,&b.y);
    printf("distance is %lf\n",getDistance(a,b));
    return 0;
}